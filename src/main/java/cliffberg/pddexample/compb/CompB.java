package cliffberg.pddexample.compb;

import java.util.List;

public class CompB {

	/**
	Algorithm compb-1
	*/
	public void addID(List<ID> list, ID id) {
		if (list.contains(id)) return;
		list.add(id);
	}
}
