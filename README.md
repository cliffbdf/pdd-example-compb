# pdd-example-compb

Create a list of objects, but such that when an attempt is made to add an item
that is already in the list, the attempt silently fails. The result is a list
that contains no duplicates, and in which the order of the entries is the sequence
in which each item was first encountered.

Design:
https://drive.google.com/open?id=1B9ZQ8Nn-4JMaqOR3dkf3tTDHS_yeepHciVqmu-qZrbs
